#!/usr/bin/env python

import os
import sys

PROJECT_ROOT = os.path.abspath(".")

THREEPS_ROOT = os.path.abspath(
    os.path.join(PROJECT_ROOT, "..", "tools")
    )
GAMEPLAY_ROOT = os.path.abspath(
    os.path.join(PROJECT_ROOT, "..", "GamePlay")
    )

sys.path.append(
    os.path.join(THREEPS_ROOT, "waf")
    )


NAME = "lfant"
VERSION = "0.1.5"

project = NAME
top = PROJECT_ROOT
out = "build"

import waflib
from waflib.Task import Task
from waflib.TaskGen import extension

##########################################################################

AVAILABLE_PLATFORMS = ["linux64"]

DEFINITIONS = {
    "linux64": ["__lofi_linux__"],
    "linux": ["__lofi_linux__"]
}

LIBPATH_MAP = {
    "linux64": os.path.join("lib", "linux", "x64"),
    "linux": os.path.join("lib", "linux", "x86"),
    "macosx": os.path.join("lib", "macosx"),
    "windows": os.path.join("lib", "windows", "x86")
}

##########################################################################


def options(ctx):
    ctx.load("qt5 cxx compiler_cxx")
    ctx.add_option("--debug",
                   action="store_true",
                   default=False,
                   help="Enable the debug build.")
    ctx.add_option("--platform",
                   action="store",
                   default="linux64",
                   help="One of the following: [%s]" % ", ".join(AVAILABLE_PLATFORMS))
    ctx.add_option("--enable-tests",
                   action="store_true",
                   default=False,
                   help="Build any tests setup for this project. (currently unimplemented)")


def configure (ctx):
    # we want to default to the clang++ compiler unless CXX is already defined
    if "CXX" not in os.environ:
        os.environ["CXX"] = "clang++"
    ctx.load("compiler_cxx")

    # we need to exert a little influence over the qt configuration
    ctx.options.qtdir = os.path.join(
        THREEPS_ROOT, ctx.options.platform, "QT", "current"
        )
    os.environ["PKG_CONFIG_PATH"] = os.path.join(ctx.options.qtdir, "lib/pkgconfig")
    ctx.load("qt5")

    ctx.env.build_tests = ctx.options.enable_tests
    ctx.env.platform = ctx.options.platform
    if ctx.env.platform not in AVAILABLE_PLATFORMS:
        raise waflib.Errors.ConfigurationError(
            "%s is not an available platform." % ctx.env.platform
            )

    ctx.env.debug = ctx.options.debug
    if ctx.env.debug:
        ctx.env.append_unique("CXXFLAGS", ["-g"])
        ctx.env.append_unique("CXXFLAGS", ["-D_DEBUG"])

    #################################################################
    # third party libs setup

    ctx.env.LIBPATH_COMMON = [
        os.path.join(THREEPS_ROOT, ctx.env.platform, "lib")
    ]
    ctx.env.INCLUDES_COMMON = [
        os.path.join(THREEPS_ROOT, "include"),
        os.path.join(THREEPS_ROOT, ctx.env.platform, "include"),
        os.path.join(THREEPS_ROOT, ctx.env.platform, "include", "gameplay")
    ]

    poco_libs = ["PocoUtil", "PocoNet", "PocoXML", "PocoFoundation"]
    if ctx.env.debug:
        poco_libs = ["%sd" % libname for libname in poco_libs]

    ctx.env.LIB_COMMON = [
        "SDL2", "gameplay", "m", "GL"
    ] + poco_libs

    ctx.env.STLIB_COMMON = [
        "lua", "BulletDynamics", "BulletCollision", "LinearMath", 
        "vorbisfile", "vorbis", "ogg", "openal", "png", "z"
    ]

    ctx.env.RPATH_COMMON = [
        ".", "./%s" % out, os.path.join(THREEPS_ROOT, ctx.env.platform, "lib")
    ]

    # end of third party libs setup
    #################################################################


def build(bld):
    libs = [
        "COMMON",
        #"QT5WIDGETS",
        "QT5GUI",
        "QT5CORE"
        ]

    common_cxxflags = ['-std=c++11', '-fPIC']

    lofi_src_list = bld.path.ant_glob("src/lib/LF/*.cpp")
    lfant_src_list = bld.path.ant_glob("src/*.cpp")
    game_src_list = bld.path.ant_glob("src/TestGame/*.cpp")

    liblofi = bld.stlib(
        features=["cxx"],
        source=lofi_src_list,
        includes=["src/lib"],
        defines=DEFINITIONS[ bld.env.platform ],
        target="lofi",
        cxxflags=common_cxxflags,
        use=["COMMON"]
        )

    lfant = bld.program(
        features=["cxx"],
        source=lfant_src_list,
        includes=["src"],
        defines=DEFINITIONS[ bld.env.platform ],
        target=NAME,
        cxxflags=common_cxxflags,
        use=libs
        )

    mesh_game = bld.shlib(
        features=["cxx"],
        source=game_src_list,
        includes=["src/TestGame", "src/lib"],
        defines=DEFINITIONS[ bld.env.platform ],
        target="testgame",
        cxxflags=common_cxxflags,
        use=libs + ["lofi"]
        )


def dist (ctx):
    ctx.excl = '**/.waf-* **/.lock-* **/*~ **/*.swp \
                **/build/* **/private/* \
                **/*.pyc **/*.o **/.git/* **/.gitignore \
                ***/*.iml ***/.idea'
