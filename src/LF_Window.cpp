#include "LF_Window.h"

#include <assert.h>
#include <iostream>

#include <QtGui/QScreen>
#include <QtGui/QPixmap>

LF::Window::Window(QWindow* parent)
  : QWindow(parent),
    m_blank_cursor(nullptr),
    m_default_cursor(nullptr)
{
  setSurfaceType(QWindow::OpenGLSurface);

  auto blank_pixmap = new QPixmap(8, 8);
  blank_pixmap->fill(Qt::transparent);
  m_blank_cursor = new QCursor( *blank_pixmap );

  m_default_cursor = new QCursor(Qt::ArrowCursor);
}

LF::Window::~Window() {
}

void LF::Window::initialize() {
  initializeOpenGLFunctions();
}

void LF::Window::setEventCallback(std::function<bool (QEvent*)> cb) {
  m_event_callback = cb;
}

bool LF::Window::event(QEvent *event) {
  bool result = false;

  if (m_event_callback) {
    result = m_event_callback(event);
  }

  if (!result) {
    result = QWindow::event(event);
  }

  return result;
}

void LF::Window::setCursorVisible(bool visible) {
  if (visible) {
    setCursor(*m_default_cursor);
  } else {
    setCursor(*m_blank_cursor);
  }
}

#include "LF_Window.moc"
