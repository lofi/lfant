#include "Client.h"
#include "World.h"
#include "Input.h"
#include "Event.h"
#include "Command.h"

#include <Poco/Observer.h>

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_gamecontroller.h>

using Poco::Observer;

using gameplay::Keyboard;
using gameplay::Mouse;
using gameplay::Node;
using gameplay::Model;
using gameplay::Scene;
using gameplay::Camera;
using gameplay::ScreenDisplayer;

using LF::Input;
using LF::Command;
using LF::CommandNotification;

int gamepadButton(const SDL_GameControllerButton &sdl_button);

LF::Client client;

LF::Client::Client() {
  // pass
}

LF::Client::~Client() {
  // pass
}

void LF::Client::keyEvent(Keyboard::KeyEvent gp_event, int key) {
  const Command* command = Input::binding(LF::KEYBOARD, key);
  if (command != nullptr) {
    LF::Event* event = new LF::Event;
    event->source = LF::KEYBOARD;
    event->item = key;
    event->data.pressed = (gp_event == Keyboard::KEY_PRESS);

    command->notify(event);
  }
}

bool LF::Client::mouseEvent(Mouse::MouseEvent event, int x, int y, int wheel_delta) {
  switch(event) {
    case Mouse::MOUSE_MOVE: {
      const Command* command = Input::binding(LF::MOUSE_MOTION);
      if (command != nullptr) {
        LF::Event* event = new LF::Event;
        event->source = LF::MOUSE_MOTION;
        event->data.ivalues.x = x;
        event->data.ivalues.y = y;

        command->notify(event);
      }
      break;
    }
      
    default: {} break;
  }
  return true;
}

void LF::Client::initialize() {
  Command::sm_notification_center.addObserver(
    Poco::Observer<LF::Client, CommandNotification>((*this), &LF::Client::__execCommand)
    );
  
  startup(this);
}

void LF::Client::finalize() {
  Command::sm_notification_center.removeObserver(
    Poco::Observer<LF::Client, CommandNotification>((*this), &LF::Client::__execCommand)
    );
  
  if (m_current_world) {
    m_current_world->finalize();
  }
  shutdown(this);
}

void LF::Client::update(float elapsed_time) {
  gameplay::Vector2 axis_values;

  // Using the SDL event system for button activity rather
  // than roll my own and duplicate this functionality
  // TODO: move gamepad button events into gameplay::Game?
  SDL_Event sdl_event;
  while( SDL_PollEvent(&sdl_event) ) {
    switch (sdl_event.type) {
      case SDL_CONTROLLERBUTTONUP:
      case SDL_CONTROLLERBUTTONDOWN: {
        int button = gamepadButton(sdl_event.cbutton.button);
        const Command* command = Input::binding(LF::GAMEPAD, button);
        if (command != nullptr) {
          Event* lf_event = new Event;
          lf_event->source = LF::GAMEPAD;
          lf_event->index = sdl_event.cbutton.which;
          lf_event->item = button;
          lf_event->data.pressed = (sdl_event.cbutton.state == SDL_PRESSED);

          command->notify(lf_event);
        }
      } break;
    }
  }

  // since axis values more or less need to be continuous we just always send
  // notification if there is a binding.
  // TODO: all of this shit could be handled better right?
  auto notifyForAxis = [&axis_values] (gameplay::Gamepad* gamepad, int index, int axis) {
    const Command* command = Input::binding(LF::GAMEPAD, axis);
    if (command != nullptr) {
      gamepad->getJoystickValues(axis, &axis_values);
      
      Event* event = new Event;
      event->source = LF::GAMEPAD;
      event->index = index;
      event->item = LF::GAMEPAD_LEFT_STICK;
      event->data.fvalues.x = axis_values.x;
      event->data.fvalues.y = axis_values.y;

      command->notify(event);
    }
  };
  
  for (int i=0; i < getGamepadCount(); i++) {
    gameplay::Gamepad* gamepad = getGamepad(i);
    notifyForAxis(gamepad, i, LF::GAMEPAD_LEFT_STICK);
    notifyForAxis(gamepad, i, LF::GAMEPAD_RIGHT_STICK);
  }
  
  
  if (!m_current_world) {
    return;
  }
  m_current_world->update(elapsed_time);
}

void LF::Client::render(float elapsed_time) {
  // Clear the color and depth buffers.
  clear(CLEAR_COLOR_DEPTH, gameplay::Vector4::zero(), 1.0f, 0);
  
  GP_ASSERT(m_current_world);
  if (!m_current_world) {
    return;
  }

  m_current_world->preRender(elapsed_time);
  
  const gameplay::Scene* scene = m_current_world->scene();
  if (scene == nullptr) {
    return;
  }

  // view frustum culling
  auto node = scene->getFirstNode();
  while (node) {
    buildPVS(node, scene->getActiveCamera());
    node = node->getNextSibling();
  }

  // TODO: Might have to create two sets so that transparent
  // nodes are rendered separately. 
  for (auto model : m_visible_set) {
    model->draw();
  }
  
  m_current_world->postRender(elapsed_time);

  m_visible_set.clear();
}

const std::shared_ptr<LF::World> LF::Client::world() const {
  return m_current_world;
}

void LF::Client::setWorld(std::shared_ptr<World> world) {
  if (m_current_world) {
    m_current_world->finalize();
  }

  m_current_world = world;
  displayScreen(this, &LF::Client::__drawWorldLoadingScreen, nullptr, 1000L);
  m_current_world->initialize();
}

void LF::Client::buildPVS(const Node* node, const Camera* camera) {
  auto model = node->getModel();
  if (model != nullptr) {
    if (node->hasTag("no_cull") ||
        node->getBoundingSphere().intersects( camera->getFrustum() )) {
      m_visible_set.push_back(model);

      auto child = node->getFirstChild();
      while (child != nullptr) {
        buildPVS(child, camera);

        child = child->getNextSibling();
      }
    }
  }
}

void LF::Client::__drawWorldLoadingScreen(void *param) {
  m_current_world->loadingScreen(param);
}

void LF::Client::__execCommand(CommandNotification* notification) {
  (*notification->command)(notification->event);
  notification->release();
}

int gamepadButton(const SDL_GameControllerButton &sdl_button) {
  switch (sdl_button) {
    case SDL_CONTROLLER_BUTTON_A: return gameplay::Gamepad::BUTTON_A;
    case SDL_CONTROLLER_BUTTON_B: return gameplay::Gamepad::BUTTON_B;
    case SDL_CONTROLLER_BUTTON_X: return gameplay::Gamepad::BUTTON_X;
    case SDL_CONTROLLER_BUTTON_Y: return gameplay::Gamepad::BUTTON_Y;
    case SDL_CONTROLLER_BUTTON_BACK: return gameplay::Gamepad::BUTTON_MENU1;
    case SDL_CONTROLLER_BUTTON_GUIDE: return gameplay::Gamepad::BUTTON_MENU2;
    case SDL_CONTROLLER_BUTTON_START: return gameplay::Gamepad::BUTTON_MENU3;
    case SDL_CONTROLLER_BUTTON_DPAD_UP: return gameplay::Gamepad::BUTTON_UP;
    case SDL_CONTROLLER_BUTTON_DPAD_DOWN: return gameplay::Gamepad::BUTTON_DOWN;
    case SDL_CONTROLLER_BUTTON_DPAD_LEFT: return gameplay::Gamepad::BUTTON_LEFT;
    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT: return gameplay::Gamepad::BUTTON_RIGHT;
    case SDL_CONTROLLER_BUTTON_LEFTSHOULDER: return gameplay::Gamepad::BUTTON_L1;
    case SDL_CONTROLLER_BUTTON_LEFTSTICK: return gameplay::Gamepad::BUTTON_L3;
    case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER: return gameplay::Gamepad::BUTTON_R1;
    case SDL_CONTROLLER_BUTTON_RIGHTSTICK: return gameplay::Gamepad::BUTTON_R3;
    default: return -1;
  }
}
