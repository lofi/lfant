#ifndef _LF_WORLD_H_
#define _LF_WORLD_H_

#include <gameplay.h>

namespace LF {
  class World
  {
  public:
    World();
    virtual ~World();

    const gameplay::Scene* scene();

    virtual void initialize();
    virtual void finalize();

    virtual void update(float elapsed_time);

    virtual void preRender(float elapsed_time);
    virtual void postRender(float elapsed_time);

    virtual void loadingScreen(void *param);

  protected:
    gameplay::Scene* m_scene;

    void enableGrid(bool enable);

  private:
    static gameplay::Model* createGridModel(unsigned int lineCount = 41);
  };
}

#endif /* _LF_WORLD_H_ */
