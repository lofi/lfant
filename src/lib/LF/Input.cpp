#include "Input.h"
#include "Command.h"

using LF::Command;

LF::Input::binding_t LF::Input::sm_bindings;

void LF::Input::bind(const Command &cmd,
                     const LF::EventSource &source,
                     const int &component) {
  sm_bindings[ bindSource_t(source, component) ] = &cmd;
}

const Command* LF::Input::binding(const LF::EventSource &source,
                            const int &component) {
  bindSource_t bind_source = {source, component};
  auto command = sm_bindings.find(bind_source);
  if (command == sm_bindings.end()) {
    return nullptr;
  } else {
    return command->second;
  }
}
