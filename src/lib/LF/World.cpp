#include "World.h"

using gameplay::Node;
using gameplay::Model;
using gameplay::Mesh;
using gameplay::VertexFormat;
using gameplay::Vector4;

LF::World::World()
  : m_scene(nullptr)
{
  // pass
}

LF::World::~World() {
  if (m_scene != nullptr) {
    SAFE_RELEASE(m_scene);
  }
}

const gameplay::Scene* LF::World::scene() {
  return m_scene;
}

void LF::World::initialize() {
  // pass
}

void LF::World::finalize() {
  // pass
}

void LF::World::update(float elapsed_time) {
  // pass
}

void LF::World::preRender(float elapsed_time) {
  // pass
}

void LF::World::postRender(float elapsed_time) {
  // pass
}

void LF::World::loadingScreen(void* param) {
  // pass
}

void LF::World::enableGrid(bool enable) {
  Node *grid_node = m_scene->findNode("grid");
  if (enable && grid_node == nullptr) {
    Model* grid_model = createGridModel();

    grid_node = m_scene->addNode("grid");
    grid_node->setTag("no_cull");
    grid_node->setModel(grid_model);
    grid_model->release();
  } else if (!enable && grid_node != nullptr) {
    m_scene->removeNode(grid_node);
    grid_node->release();
  }
}

Model* LF::World::createGridModel(unsigned int lineCount)
{
  // There needs to be an odd number of lines
  lineCount |= 1;
  const unsigned int pointCount = lineCount * 4;
  const unsigned int verticesSize = pointCount * (3 + 3);  // (3 (position(xyz) + 3 color(rgb))
  
  std::vector<float> vertices;
  vertices.resize(verticesSize);
  
  const float gridLength = (float)(lineCount / 2);
  float value = -gridLength;
  for (unsigned int i = 0; i < verticesSize; ++i)
  {
    // Default line color is dark grey
    Vector4 color(0.3f, 0.3f, 0.3f, 1.0f);
    
    // Very 10th line is brighter grey
    if (((int)value) % 10 == 0)
    {
      color.set(0.45f, 0.45f, 0.45f, 1.0f);
    }
    
    // The Z axis is blue
    if (value == 0.0f)
    {
      color.set(0.15f, 0.15f, 0.7f, 1.0f);
    }
    
    // Build the lines
    vertices[i] = value;
    vertices[++i] = 0.0f;
    vertices[++i] = -gridLength;
    vertices[++i] = color.x;
    vertices[++i] = color.y;
    vertices[++i] = color.z;
    
    vertices[++i] = value;
    vertices[++i] = 0.0f;
    vertices[++i] = gridLength;
    vertices[++i] = color.x;
    vertices[++i] = color.y;
    vertices[++i] = color.z;
    
    // The X axis is red
    if (value == 0.0f)
    {
      color.set(0.7f, 0.15f, 0.15f, 1.0f);
    }
    vertices[++i] = -gridLength;
    vertices[++i] = 0.0f;
    vertices[++i] = value;
    vertices[++i] = color.x;
    vertices[++i] = color.y;
    vertices[++i] = color.z;
    
    vertices[++i] = gridLength;
    vertices[++i] = 0.0f;
    vertices[++i] = value;
    vertices[++i] = color.x;
    vertices[++i] = color.y;
    vertices[++i] = color.z;
    
    value += 1.0f;
  }
  VertexFormat::Element elements[] =
    {
      VertexFormat::Element(VertexFormat::POSITION, 3),
      VertexFormat::Element(VertexFormat::COLOR, 3)
    };
  Mesh* mesh = Mesh::createMesh(VertexFormat(elements, 2), pointCount, false);
  if (mesh == NULL)
  {
    return NULL;
  }
  mesh->setPrimitiveType(Mesh::LINES);
  mesh->setVertexData(&vertices[0], 0, pointCount);
  
  Model* model = Model::create(mesh);
  model->setMaterial("res/grid.material");
  SAFE_RELEASE(mesh);
  return model;
}
