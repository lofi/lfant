#ifndef _LF_COMMAND_H_
#define _LF_COMMAND_H_

#include <list>
#include <string>
#include <memory>
#include <map>
#include <Poco/AutoPtr.h>
#include <Poco/Notification.h>
#include <Poco/NotificationCenter.h>
#include <Poco/Observer.h>

#include "Event.h"

namespace LF {

  ////////////////////////////////////////////////////////////////////////////

  class Command
  {
    friend class Client;
  public:
    typedef std::function<void (Event*)> delegateFn_t;
    
    Command();
    ~Command();

    const std::string& name();

    void addDelegate(const delegateFn_t& delegate_fn);
    void clearDelegates();
    void operator()(Event*) const;

    void notify(Event*) const;

  private:
    std::list<delegateFn_t> m_delegate_list;
    std::string m_name;

    ////////// static interface ///////////
  public:
    static Poco::NotificationCenter& notificationCenter();
    
  private:
    static Poco::NotificationCenter sm_notification_center;
  };

  ////////////////////////////////////////////////////////////////////////////

  class CommandNotification : public Poco::Notification {
  public:

    CommandNotification(const Command* cmd, Event* e)
      : command(cmd), event(e) {
    }
    
    ~CommandNotification() {
      command = nullptr;
      delete event;
    }
    
    const Command *command;
    Event *event;
  };
  
}

#define LOFI_COMMAND(name) LF::Command name;

#endif /* _LF_COMMAND_H_ */
