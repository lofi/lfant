#ifndef _LF_EVENT_H_
#define _LF_EVENT_H_

namespace LF {

  enum EventSource {
    MOUSE=0,
    MOUSE_MOTION,
    GAMEPAD,
    KEYBOARD,
    APPLICATION
  };

  enum GamepadSticks {
    GAMEPAD_LEFT_STICK=0,
    GAMEPAD_RIGHT_STICK,
    GAMEPAD_LEFT_STICK_X,
    GAMEPAD_LEFT_STICK_Y,
    GAMEPAD_RIGHT_STICK_X,
    GAMEPAD_RIGHT_STICK_Y,
  };

  enum GamepadTriggers {
    GAMEPAD_LEFT_TRIGGER=0,
    GAMEPAD_RIGHT_TRIGGER
  };

  typedef struct {
    float x;
    float y;
    float z;
  } vectorf_t;

  typedef struct {
    int x;
    int y;
    int z;
  } vectori_t;

  typedef union {
    bool pressed;
    vectori_t ivalues;
    vectorf_t fvalues;
  } eventData_t;

  struct Event {
    EventSource source;
    int item;
    int index;
    eventData_t data;
  };
  
}

#endif /* _LF_EVENT_H_ */
