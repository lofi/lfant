#ifndef _LF_CLIENT_H_
#define _LF_CLIENT_H_

#include <memory>
#include <vector>
#include <gameplay.h>

namespace LF {

  class World;
  class CommandNotification;

  class Client : public gameplay::Game
  {
  public:
    Client();
    virtual ~Client();

    void keyEvent(gameplay::Keyboard::KeyEvent event, int key);
    bool mouseEvent(gameplay::Mouse::MouseEvent event, int x, int y, int wheel_delta);

    const std::shared_ptr<World> world() const;
    void setWorld(std::shared_ptr<World> next_world);

    // These are to be provided by the game plugin
    static std::function<void (Client*)> startup;
    static std::function<void (Client*)> shutdown;

  protected:
    void initialize();
    void finalize();

    void update(float elapsed_time);

    void render(float elapsed_time);

  private:
    std::vector<gameplay::Model*> m_visible_set;
    std::shared_ptr<World> m_current_world;

    void buildPVS(const gameplay::Node* node, const gameplay::Camera* camera);
    void __drawWorldLoadingScreen(void *param);
    void __execCommand(CommandNotification* cmd_notification);
  };

}

#endif /* _LF_CLIENT_H_ */
