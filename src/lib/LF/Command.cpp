#include "Command.h"

////////////////////////////////////////////////////////////////////////////////
// Command

LF::Command::Command()
{
  // pass
}

LF::Command::~Command() {
  clearDelegates();
}

void LF::Command::addDelegate(const delegateFn_t& delegate_fn) {
  m_delegate_list.push_back( std::move(delegate_fn) );
}

void LF::Command::clearDelegates() {
  m_delegate_list.clear();
}

void LF::Command::operator()(Event* event) const {
  for (auto delegate : m_delegate_list) {
    delegate(event);
  }
}

void LF::Command::notify(Event* event) const {
  LF::Command::sm_notification_center.postNotification(
    new LF::CommandNotification(this, event)
    );
}

////////// static interface ///////////

Poco::NotificationCenter& LF::Command::notificationCenter() {
  return sm_notification_center;
}
Poco::NotificationCenter LF::Command::sm_notification_center;
