#ifndef _LF_INPUT_H_
#define _LF_INPUT_H_

#include <gameplay.h>

#include "Event.h"

namespace LF {

  class Command;

  class Input {
  public:

    typedef std::pair<EventSource, int> bindSource_t;
    typedef std::map<bindSource_t, const Command*> binding_t;
    
    static void bind(const Command& cmd,
                     const EventSource& source,
                     const int& component=0);

    static const Command* binding(const EventSource& source,
                                  const int& component=0);

  private:
    static binding_t sm_bindings;
  };
  
} // end of namespace LF

#endif /* _LF_INPUT_H_ */
