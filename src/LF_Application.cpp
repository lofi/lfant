
#include <memory>
#include <cstdlib>
#include <iostream>

#include <Poco/AutoPtr.h>
#include <Poco/Logger.h>
#include <Poco/Channel.h>
#include <Poco/Message.h>
#include <Poco/FileChannel.h>
#include <Poco/Formatter.h>
#include <Poco/PatternFormatter.h>
#include <Poco/FormattingChannel.h>
#include <Poco/Util/Subsystem.h>
#include <Poco/Util/LayeredConfiguration.h>
#include <Poco/Util/HelpFormatter.h>
#include <Poco/SharedLibrary.h>

#include <QtCore/QCoreApplication>

#include <SDL2/SDL.h>

#include "gameplay.h"

#include "LF_Application.h"
#include "LF_Window.h"

using Poco::Util::LayeredConfiguration;
using Poco::Util::Subsystem;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Logger;
using Poco::Channel;
using Poco::FileChannel;
using Poco::PatternFormatter;
using Poco::FormattingChannel;
using Poco::Message;
using Poco::Util::HelpFormatter;
using Poco::AutoPtr;
using Poco::SharedLibrary;

LFApplication::LFApplication() {
  // pass
}

LFApplication::~LFApplication(){
  // pass
}


int LFApplication::main(const std::vector<std::string>& args) {
  int result = Poco::Util::Application::EXIT_OK;
  
  if (!config().hasOption("lofi.game")) {

    logger().fatal("No game specified!");
    result = Poco::Util::Application::EXIT_CANTCREAT;
    
  } else {

    SharedLibrary game_sl;
    game_sl.load(config().getString("lofi.game"));
    
    if ( game_sl.hasSymbol("client") ) {
      
      std::string resource_path;
      if (config().has("lofi.resourcepath")) {
        resource_path = config().getString("lofi.resourcepath");
      } else {
        // TODO: use the directory of the game lib
      }
      
      gameplay::FileSystem::setResourcePath( resource_path.c_str() );
      
      // Loading the shared lib should mean that a global game object singleton already
      // exists and can now be used by the platform implementation.
      gameplay::Game* game = gameplay::Game::getInstance();
      GP_ASSERT(game != nullptr);
      
      std::unique_ptr<gameplay::Platform> platform( 
        gameplay::Platform::create(game, nullptr) 
        );
      GP_ASSERT(platform.get());
      
      // main loop
      platform->enterMessagePump();
      
    } else {
      
      logger().fatal("\"client\" has not been defined in specified library.");
      result = Poco::Util::Application::EXIT_DATAERR;
      
    }
  }

  return result;
}


void LFApplication::initialize(Poco::Util::Application &self) {
  setConfigurationDefaults();
  loadConfiguration();
  initLogging();

  int rc = SDL_Init( SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC );
  if (rc != 0) {
    logger().fatal(SDL_GetError());
    // TODO: throw an exception here.
  }

  SDL_GameControllerEventState(SDL_ENABLE);

  Poco::Util::Application::initialize(self);
}


void LFApplication::uninitialize() {
  SDL_Quit();

  Poco::Util::Application::uninitialize();
}


void LFApplication::setConfigurationDefaults() {
  if (!config().has("lofi.resourcepath")) {
    config().setString("lofi.resourcepath", "./");
  }
  if (!config().has("lofi.logfile")) {
    config().setString("lofi.logfile", "lfant.log");
  }
}


int LFApplication::loadConfiguration() {
  const char* name_key = "name";
  const char* guid_key = "guid";

  gameplay::Properties *gamecontrollers = gameplay::Properties::create("gamecontrollers.conf");

  if (gamecontrollers != NULL) {
    std::stringstream hint;

    gameplay::Properties *controller_profile = gamecontrollers->getNextNamespace();
    while (controller_profile != NULL) {
      const char *name = controller_profile->getString(name_key);
      const char *guid = controller_profile->getString(guid_key);

      hint << guid << "," << name;

      const char* key = controller_profile->getNextProperty();
      while (key != NULL) {
        if (std::strcmp(key, name_key) && std::strcmp(key, guid_key)) {
          hint << "," << key << ":" << controller_profile->getString(key);
        }

        key = controller_profile->getNextProperty();
      }

      hint << std::endl;

      controller_profile = gamecontrollers->getNextNamespace();
    }
    SDL_bool rc = SDL_SetHint(SDL_HINT_GAMECONTROLLERCONFIG, hint.str().c_str());
    if (rc == SDL_FALSE) {
      std::cerr << "Unable to set gamecontroller hints: " << SDL_GetError() << std::endl;
    }
  }

  return Poco::Util::Application::loadConfiguration();
}


void LFApplication::initLogging() {
  Poco::Logger& logger = Poco::Logger::get("lofi");
  AutoPtr<FileChannel> output_channel(new FileChannel());

  AutoPtr<PatternFormatter> pattern_formatter(new PatternFormatter());
  pattern_formatter->setProperty("pattern", "%Y/%m/%d | %H:%M:%S | %p | %s | %t");

  output_channel->setProperty("path", config().getString("lofi.logfile"));
  output_channel->setProperty("rotation", "never");
  output_channel->setProperty("archive", "timestamp");

  AutoPtr<FormattingChannel> formatting_channel(
    new FormattingChannel(pattern_formatter,
    output_channel)
    );

  logger.setChannel(formatting_channel);
  logger.setLevel(Message::PRIO_DEBUG);

  setLogger( Poco::Logger::get("lofi.application") );
}


void LFApplication::defineOptions(OptionSet& options) {
  options.addOption(
    Option("game","",
           "Path to the shared library implementing the game you wish to play.",
           false,
           "<path to shared library>",
           true)
    .repeatable(false)
    .binding("lofi.game")
    );
  options.addOption(
    Option("resources","",
           "Path to the games resources.",
           false,
           "<path to resource folder>",
           true)
    .repeatable(false)
    .binding("lofi.resourcepath")
    );
  options.addOption(
    Option("help", "h",
           "Display usage information.",
           false)
    .repeatable(false)
    );
  Poco::Util::Application::defineOptions(options);
}


void LFApplication::handleOption(const std::string &name, const std::string &value) {
  if (name == "help") {
    displayHelp();
    stopOptionsProcessing();
    std::exit(Application::EXIT_USAGE);
  }
  Poco::Util::Application::handleOption(name, value);
}


void LFApplication::displayHelp() {
  HelpFormatter help(options());
  help.setCommand(commandName());
  help.setUsage("OPTIONS");
  help.setHeader("DO YOU WANT TO PLAY A GAME?");
  help.format(std::cout);
}
