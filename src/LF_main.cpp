#ifdef __lofi_linux__

#include <QtGui/QGuiApplication>
#include "LF_Application.h"

int main(int argc, char *argv[]) {
  QGuiApplication q_gui_app(argc, argv);
  
  LFApplication app;
  app.init(argc, argv);
  return app.run();
}

#endif
