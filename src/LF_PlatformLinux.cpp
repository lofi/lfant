#ifdef __lofi_linux__

#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstdarg>
#include <climits>

#include <time.h>

#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>
#include <QtGui/QSurfaceFormat>
#include <QtGui/QOpenGLContext>
#include <QtGui/QMouseEvent>
#include <QtGui/QScreen>

#include <Poco/Logger.h>
#include <Poco/Format.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_gamecontroller.h>
#include <SDL2/SDL_joystick.h>
#include <SDL2/SDL_mouse.h>
#include <SDL2/SDL_events.h>

#include "Base.h"
#include "Platform.h"
#include "FileSystem.h"
#include "Game.h"
#include "Form.h"
#include "ScriptController.h"
#include "Gamepad.h"

#include "LF_Window.h"

#define TOUCH_COUNT_MAX 4

///////////////////////////////////////////////////////////
// Globals
struct timespec __timespec;
static double __time_absolute = 0.0;
static double __time_start = 0.0;

LF::Window *__window = nullptr;
static int __window_center[2] = {0, 0};
static bool __mouse_captured = false;
static bool __cursor_visible = true;
static bool __vsync = false;
QOpenGLContext* __context = nullptr;

// accelerometer values
static float __pitch = 0.0f;
static float __roll = 0.0f;

static Poco::Logger* __logger = nullptr;

static const float AXIS_VALUE_MAX = 32767.0f;

//////////////////////////////////////////////////////////
// The gamecontroller_t structure is used to correlate
// gameplay gamepads and SDL gamecontrollers.
typedef struct _GAMECONTROLLER_T
{
  SDL_GameController* controller;
  gameplay::Gamepad* gamepad;

  _GAMECONTROLLER_T(SDL_GameController* gc, gameplay::Gamepad* gp) :
  controller(gc), gamepad(gp)
  {}
  _GAMECONTROLLER_T(const _GAMECONTROLLER_T &other) {
    controller = other.controller;
    gamepad = other.gamepad;
  }
} gamecontroller_t;

static std::vector<gamecontroller_t> __controllers;
//
/////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////
// Forward declarations
gameplay::Keyboard::Key getKey(const QKeyEvent* event);
gameplay::Mouse::MouseEvent getMouseButtonEvent(const QMouseEvent* event, bool pressed);

/////////////////////////////////////////////////////////
// Implementation
namespace gameplay
{
  extern void print(const char* format, ...) {
    #ifdef _DEBUG
    static Poco::Logger& logger = Poco::Logger::get("lofi.debug_output");
    
    GP_ASSERT(format);

    char buffer[1024] = {'\0'};
    std::va_list argptr;
    va_start(argptr, format);
    std::vsprintf(buffer, format, argptr);
    std::cerr << buffer << std::endl;
    va_end(argptr);

    logger.debug(buffer);
    #endif
  }

  double timespec2millis(struct timespec *a) {
      GP_ASSERT(a);
      return (1000.0 * a->tv_sec) + (0.000001 * a->tv_nsec);
  }

  Platform::Platform(Game* game) : _game(game) {
    // pass
  }

  Platform::~Platform() {
    // pass
  }

  Platform* Platform::create(Game* game, void* attachToWindow) {
    __logger = &Poco::Logger::get("lofi.platform");

    GP_ASSERT(game);
    Platform* platform = NULL;

    int joystick_count = SDL_NumJoysticks();
    for (int i=0; i < joystick_count; i++) {
      if (SDL_IsGameController(i)) {
        gamecontroller_t gc( gamecontroller_t( SDL_GameControllerOpen(i), NULL ) );

        int button_count = SDL_CONTROLLER_BUTTON_MAX;
        int joystick_count = SDL_CONTROLLER_AXIS_MAX;
        int trigger_count = 2;
        int vendor_id = 0;
        int product_id = 0;
        const char* vendor_string = "unknown";
        const char* product_string = SDL_GameControllerName(gc.controller);
        print("Gamepad found: %s", product_string);

        gc.gamepad = Gamepad::add(i, button_count, joystick_count, trigger_count,
                                  vendor_id, product_id, vendor_string, product_string);

        __controllers.push_back(gc);
      }
    }

    std::string window_title = "LOFI Game";
    int window_width = 1280;
    int window_height = 720;
    bool window_fullscreen = false;

    if (game->getConfig()) {
      gameplay::Properties* window_config = game->getConfig()->getNamespace("window", true);
      if (window_config->exists("title")) {
        window_title = window_config->getString("title");
      }
      if (window_config->exists("width")) {
        window_width = window_config->getInt("width");
      }
      if (window_config->exists("height")) {
        window_height = window_config->getInt("height");
      }
      if (window_config->exists("fullscreen")) {
        window_fullscreen = window_config->getBool("fullscreen");
      }
    }

    QSurfaceFormat format;
    format.setSamples(4);
    __context = new QOpenGLContext();
    __context->setFormat(format);
    __context->create();

    __window = new LF::Window();

    __window->setTitle(window_title.c_str());
    __window->setWidth(window_width);
    __window->setHeight(window_height);

    __window_center[0] = __window->width() / 2;
    __window_center[1] = __window->height() / 2;
    
    if (window_fullscreen) {
      __window->showFullScreen();
    } else {
      __window->show();
      QScreen* screen = __window->screen();
      __window->setPosition(( screen->geometry().width()  - __window->width()  ) / 2, 
                            ( screen->geometry().height() - __window->height() ) / 2);
    }

    __context->makeCurrent(__window);
    __window->initialize();

    SDL_DisableScreenSaver();

    platform = new Platform(game);

    return platform;
  }

  void __shutdown() {
    for (gamecontroller_t gc : __controllers) {
      SDL_GameControllerClose(gc.controller);
    }
    SDL_EnableScreenSaver();
  }

  int Platform::enterMessagePump() {
    int result = 0;

    GP_ASSERT(_game);
    GP_ASSERT(__window);
    GP_ASSERT(__logger);

    // Get the initial time
    clock_gettime(CLOCK_REALTIME, &__timespec);
    __time_start = timespec2millis(&__timespec);
    __time_absolute = 0L;

    // Get the window up.
    qApp->processEvents();
    qApp->sendPostedEvents();


    // Boot it up!
    _game->run();

    // If the game has captured the mouse then we should warp the cursor
    __window->cursor().setPos(__window_center[0], __window_center[1]);


    // In some platforms we poll for input events. 
    // With Qt we just hook into the windows event callback.
    bool mouse_pressed = false;
    __window->setEventCallback([&] (QEvent* event) -> bool {
      bool result = false;

      switch(event->type()) {

        case QEvent::Resize: {
          glViewport(0, 0, __window->width(), __window->height());
          __window_center[0] = __window->width() / 2;
          __window_center[1] = __window->height() / 2;
          result = true;
        }
        break;

        case QEvent::Close: {
          _game->exit();
          result = true;
        }
        break;

        case QEvent::KeyPress: {
          Keyboard::Key key = getKey(reinterpret_cast<QKeyEvent*>(event));
          keyEventInternal(Keyboard::KEY_PRESS, key);
          result = true;
        }
        break;

        case QEvent::KeyRelease: {
          Keyboard::Key key = getKey(reinterpret_cast<QKeyEvent*>(event));
          keyEventInternal(Keyboard::KEY_RELEASE, key);
          result = true;
        }
        break;

        case QEvent::MouseButtonPress: {
          QMouseEvent* q_mouse_event = reinterpret_cast<QMouseEvent*>(event);
          
          int x = q_mouse_event->x();
          int y = q_mouse_event->y();
          Mouse::MouseEvent mouse_event = getMouseButtonEvent(q_mouse_event, true);

          if ( !mouseEventInternal(mouse_event, x, y, 0) ) {
            touchEventInternal(Touch::TOUCH_PRESS, x, y, 0);
          }
          result = true;
          mouse_pressed = true;
        }
        break;

        case QEvent::MouseButtonRelease: {
          QMouseEvent* q_mouse_event = reinterpret_cast<QMouseEvent*>(event);
          
          int x = q_mouse_event->x();
          int y = q_mouse_event->y();
          Mouse::MouseEvent mouse_event = getMouseButtonEvent(q_mouse_event, false);

          if ( !mouseEventInternal(mouse_event, x, y, 0) ) {
            touchEventInternal(Touch::TOUCH_RELEASE, x, y, 0);
          }

          result = true;
          mouse_pressed = false;
        }
        break;

        case QEvent::MouseMove: {
          QMouseEvent *q_mouse_event = reinterpret_cast<QMouseEvent*>(event);

          QPoint cursor_position = QCursor::pos();
          int x = 0;
          int y = 0;
          if ( isMouseCaptured() ) {
            // provide relative values
            x = __window_center[0] - cursor_position.x();
            y = __window_center[1] - cursor_position.y();
            __window->cursor().setPos(__window_center[0], __window_center[1]);
          } else {
            // provide the absolute positions
            x = q_mouse_event->x();
            y = q_mouse_event->y();
          }

          if (x || y) {
            if (!mouseEventInternal(Mouse::MOUSE_MOVE, x, y, 0)) {
              if (mouse_pressed) {
                touchEventInternal(Touch::TOUCH_MOVE, x, y, 0);
              }
            }
          }

          result = true;
        }
        break;

        default: break;
      }

      return result;
    });

    
    // This timer will be triggered whenever there are no window events to process
    QTimer *idle_timer = new QTimer;
    QObject::connect(idle_timer, &QTimer::timeout, [=] () {
        SDL_PumpEvents();
        
        for (unsigned int i=0; i < Gamepad::getGamepadCount(); i++) {
          pollGamepadState(Gamepad::getGamepad(i));
        }
        
        _game->frame();
        
        swapBuffers();
        
      });
    idle_timer->start(0);


    // Enter our main event loop
    qApp->exec();

    
    delete idle_timer;

    __shutdown();

    return result;
  }

  void Platform::signalShutdown() {
    qApp->exit();
  }

  bool Platform::canExit() {
    return true;
  }

  unsigned int Platform::getDisplayWidth() {
    return __window->width();
  }

  unsigned int Platform::getDisplayHeight() {
    return __window->height();
  }

  double Platform::getAbsoluteTime() {
    clock_gettime(CLOCK_REALTIME, &__timespec);
    double now = timespec2millis(&__timespec);
    __time_absolute = now - __time_start;

    return __time_absolute;
  }

  void Platform::setAbsoluteTime(double time) {
    __time_absolute = time;
  }

  bool Platform::isVsync() {
    return __vsync;
  }

  void Platform::setVsync(bool enable) {
    __vsync = enable;
  }

  void Platform::swapBuffers() {
    __context->swapBuffers(__window);
  }

  void Platform::sleep(long ms) {
    SDL_Delay(ms);
  }

  void Platform::getAccelerometerValues(float *pitch, float *roll) {
    GP_ASSERT(pitch);
    GP_ASSERT(roll);

    *pitch = __pitch;
    *roll = __roll;
  }

  bool Platform::hasMouse() {
    return true;
  }

  void Platform::setMouseCaptured(bool captured) {
    __mouse_captured = captured;
    if (captured) {
      setCursorVisible(false);
    } else {
      setCursorVisible(true);
    }
  }

  bool Platform::isMouseCaptured() {
    return __mouse_captured;
  }

  void Platform::setCursorVisible(bool visible) {
    __window->setCursorVisible(visible);
    __cursor_visible = visible;
  }

  bool Platform::isCursorVisible() {
    return __cursor_visible;
  }

  void Platform::touchEventInternal(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex) {
      if (!Form::touchEventInternal(evt, x, y, contactIndex)) {
        Game::getInstance()->touchEvent(evt, x, y, contactIndex);
        Game::getInstance()->getScriptController()->touchEvent(evt, x, y, contactIndex);
      }
  }

  void Platform::keyEventInternal(Keyboard::KeyEvent evt, int key) {
      if (!Form::keyEventInternal(evt, key)) {
        Game::getInstance()->keyEvent(evt, key);
        Game::getInstance()->getScriptController()->keyEvent(evt, key);
      }
  }

  bool Platform::mouseEventInternal(Mouse::MouseEvent evt, int x, int y, int wheelDelta) {
      if (Form::mouseEventInternal(evt, x, y, wheelDelta)) {
        return true;
      }
      else if (Game::getInstance()->mouseEvent(evt, x, y, wheelDelta)) {
        return true;
      } else {
        return Game::getInstance()->getScriptController()->mouseEvent(evt, x, y, wheelDelta);
      }
  }

  void Platform::gamepadEventConnectedInternal(GamepadHandle handle, 
                                               unsigned int buttonCount, 
                                               unsigned int joystickCount, 
                                               unsigned int triggerCount, 
                                               unsigned int vendorId, 
                                               unsigned int productId, 
                                               const char *vendorString, 
                                               const char *productString) 
  {
    gamecontroller_t gc = __controllers[handle];
    
    gc.gamepad = gameplay::Gamepad::add(handle,
                                        SDL_CONTROLLER_BUTTON_MAX,
                                        SDL_CONTROLLER_AXIS_MAX,
                                        2,
                                        vendorId,
                                        productId,
                                        vendorString,
                                        productString);
    
    gc.controller = SDL_GameControllerOpen(handle);
  }

  void Platform::gamepadEventDisconnectedInternal(GamepadHandle handle) {
    gamecontroller_t gc = __controllers[handle];

    Gamepad::remove(handle);
    SDL_GameControllerClose(gc.controller);

    gc.controller = NULL;
    gc.gamepad = NULL;

    __controllers.erase(__controllers.begin()+handle);
  }

  void Platform::pollGamepadState(gameplay::Gamepad *gamepad) {
    gamecontroller_t gc = __controllers[gamepad->_handle];
    
    auto setButtonState = [] (unsigned int& buttons,
                              const Uint8& state,
                              const Gamepad::ButtonMapping &button)
    {
      if (state) {
        buttons |= (1ULL << button);
      } else {
        buttons &= (~(1ULL << button));
      }
    };

    auto deadzone = [] (const short& value) -> float {
      auto dz_extent = 500;
      return (value < -dz_extent || value > dz_extent) ? (float)value / AXIS_VALUE_MAX : 0.0f;
    };

    auto leftx = SDL_GameControllerGetAxis(gc.controller, SDL_CONTROLLER_AXIS_LEFTX);
    auto lefty = SDL_GameControllerGetAxis(gc.controller, SDL_CONTROLLER_AXIS_LEFTY);

    auto rightx = SDL_GameControllerGetAxis(gc.controller, SDL_CONTROLLER_AXIS_RIGHTX);
    auto righty = SDL_GameControllerGetAxis(gc.controller, SDL_CONTROLLER_AXIS_RIGHTY);

    auto triggerleft = SDL_GameControllerGetAxis(gc.controller, SDL_CONTROLLER_AXIS_TRIGGERLEFT);
    auto triggerright = SDL_GameControllerGetAxis(gc.controller, SDL_CONTROLLER_AXIS_TRIGGERRIGHT);

    gc.gamepad->_joysticks[0].x = deadzone(leftx);
    gc.gamepad->_joysticks[0].y = deadzone(lefty);

    gc.gamepad->_joysticks[1].x = deadzone(rightx);
    gc.gamepad->_joysticks[1].y = deadzone(righty);

    gc.gamepad->_triggers[0] = triggerleft;
    gc.gamepad->_triggers[1] = triggerright;

    if (triggerleft > 0.85) {
      gc.gamepad->_buttons |= (1ULL << Gamepad::BUTTON_L2);
    } else {
      gc.gamepad->_buttons &= (~(1ULL << Gamepad::BUTTON_L2));
    }

    if (triggerright > 0.85) {
      gc.gamepad->_buttons |= (1ULL << Gamepad::BUTTON_R2);
    } else {
      gc.gamepad->_buttons &= (~(1ULL << Gamepad::BUTTON_R2));
    }

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_A), 
      Gamepad::BUTTON_A
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_B), 
      Gamepad::BUTTON_B
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_X), 
      Gamepad::BUTTON_X
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_Y), 
      Gamepad::BUTTON_Y
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_START), 
      Gamepad::BUTTON_MENU3
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_GUIDE), 
      Gamepad::BUTTON_MENU2
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_BACK), 
      Gamepad::BUTTON_MENU1
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_LEFTSHOULDER), 
      Gamepad::BUTTON_L1
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_LEFTSTICK), 
      Gamepad::BUTTON_L3
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER), 
      Gamepad::BUTTON_R1
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_RIGHTSTICK), 
      Gamepad::BUTTON_R3
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_DPAD_UP), 
      Gamepad::BUTTON_UP
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_DPAD_DOWN), 
      Gamepad::BUTTON_DOWN
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_DPAD_LEFT), 
      Gamepad::BUTTON_LEFT
      );

    setButtonState(
      gc.gamepad->_buttons, 
      SDL_GameControllerGetButton(gc.controller, SDL_CONTROLLER_BUTTON_DPAD_RIGHT), 
      Gamepad::BUTTON_RIGHT
      );

  }

  bool Platform::launchURL(const char* url) {
    // pass
    return true;
  }

  ////////////////////////////////////////////
  // Unsupported platform features
  ////////////////////////////////////////////

  void Platform::displayKeyboard(bool display) {
  }

  void Platform::setMultiTouch(bool enabled) {
  }
  bool Platform::isMultiTouch() {
    return false;
  }

  bool Platform::isGestureSupported(Gesture::GestureEvent evt) {
    return false;
  }
  bool Platform::isGestureRegistered(Gesture::GestureEvent evt) {
    return false;
  }
  void Platform::registerGesture(Gesture::GestureEvent evt) {
  }
  void Platform::unregisterGesture(Gesture::GestureEvent evt) {
  }

}

gameplay::Mouse::MouseEvent getMouseButtonEvent(const QMouseEvent* event, bool pressed) {
  using gameplay::Mouse;

  switch(event->button()) {

    case Qt::LeftButton: {
      return pressed ? Mouse::MOUSE_PRESS_LEFT_BUTTON : Mouse::MOUSE_RELEASE_LEFT_BUTTON;
    }
    break;

    case Qt::MiddleButton: {
      return pressed ? Mouse::MOUSE_PRESS_MIDDLE_BUTTON : Mouse::MOUSE_RELEASE_MIDDLE_BUTTON;
    }
    break;

    case Qt::RightButton: {
      return pressed ? Mouse::MOUSE_PRESS_RIGHT_BUTTON : Mouse::MOUSE_RELEASE_RIGHT_BUTTON;
    }
    break;

    // just because?
    default: return Mouse::MOUSE_PRESS_LEFT_BUTTON;
  }
}

gameplay::Keyboard::Key getKey(const QKeyEvent* event) {
  using namespace gameplay;
  using namespace Qt;

  switch (event->key()) {
    case Key_SysReq: {
      return Keyboard::KEY_SYSREQ;
    }
    case Key_Menu: {
      return Keyboard::KEY_MENU;
    }
    case Key_Enter: {
      return Keyboard::KEY_KP_ENTER;
    }
    case Key_Pause: {
      return Keyboard::KEY_PAUSE;
    }
    case Key_ScrollLock: {
      return Keyboard::KEY_SCROLL_LOCK;
    }
    case Key_Print: {
      return Keyboard::KEY_PRINT;
    }
    case Key_Escape: {
      return Keyboard::KEY_ESCAPE;
    }
    case Key_Backspace: {
      return Keyboard::KEY_BACKSPACE;
    }
    case Key_Tab: {
      return Keyboard::KEY_TAB;
    }
    case Key_Return: {
      return Keyboard::KEY_RETURN;
    }
    case Key_CapsLock: {
      return Keyboard::KEY_CAPS_LOCK;
    }
    case Key_Shift: {
      return Keyboard::KEY_SHIFT;
    }
    case Key_Control: {
      return Keyboard::KEY_CTRL;
    }
    case Key_Alt: {
      return Keyboard::KEY_ALT;
    }
    case Key_Insert: {
      return Keyboard::KEY_INSERT;
    }
    case Key_Home: {
      return Keyboard::KEY_HOME;
    }
    case Key_PageUp: {
      return Keyboard::KEY_PG_UP;
    }
    case Key_PageDown: {
      return Keyboard::KEY_PG_DOWN;
    }
    case Key_Delete: {
      return Keyboard::KEY_DELETE;
    }
    case Key_End: {
      return Keyboard::KEY_END;
    }
    case Key_Left: {
      return Keyboard::KEY_LEFT_ARROW;
    }
    case Key_Right: {
      return Keyboard::KEY_RIGHT_ARROW;
    }
    case Key_Up: {
      return Keyboard::KEY_UP_ARROW;
    }
    case Key_Down: {
      return Keyboard::KEY_DOWN_ARROW;
    }
    case Key_NumLock: {
      return Keyboard::KEY_NUM_LOCK;
    }
    case Key_multiply: {
      return Keyboard::KEY_KP_MULTIPLY;
    }
    case Key_division: {
      return Keyboard::KEY_KP_DIVIDE;
    }
    case Key_F1: {
      return Keyboard::KEY_F1;
    }
    case Key_F2: {
      return Keyboard::KEY_F1;
    }
    case Key_F3: {
      return Keyboard::KEY_F1;
    }
    case Key_F4: {
      return Keyboard::KEY_F1;
    }
    case Key_F5: {
      return Keyboard::KEY_F1;
    }
    case Key_F6: {
      return Keyboard::KEY_F1;
    }
    case Key_F7: {
      return Keyboard::KEY_F1;
    }
    case Key_F8: {
      return Keyboard::KEY_F1;
    }
    case Key_F9: {
      return Keyboard::KEY_F1;
    }
    case Key_F10: {
      return Keyboard::KEY_F1;
    }
    case Key_F11: {
      return Keyboard::KEY_F1;
    }
    case Key_F12: {
      return Keyboard::KEY_F1;
    }
    case Key_Space: {
      return Keyboard::KEY_SPACE;
    }
    case Key_ParenRight: {
      return Keyboard::KEY_RIGHT_PARENTHESIS;
    }
    case Key_0: {
      return Keyboard::KEY_ZERO;
    }
    case Key_Exclam: {
      return Keyboard::KEY_EXCLAM;
    }
    case Key_1: {
      return Keyboard::KEY_ONE;
    }
    case Key_At: {
      return Keyboard::KEY_AT;
    }
    case Key_2: {
      return Keyboard::KEY_TWO;
    }
    case Key_NumberSign: {
      return Keyboard::KEY_NUMBER;
    }
    case Key_3: {
      return Keyboard::KEY_THREE;
    }
    case Key_Dollar: {
      return Keyboard::KEY_DOLLAR;
    }
    case Key_4: {
      return Keyboard::KEY_FOUR;
    }
    case Key_Percent: {
      return Keyboard::KEY_PERCENT;
    }
    case Key_5: {
      return Keyboard::KEY_FIVE;
    }
    case Key_6: {
      return Keyboard::KEY_SIX;
    }
    case Key_Ampersand: {
      return Keyboard::KEY_AMPERSAND;
    }
    case Key_7: {
      return Keyboard::KEY_SEVEN;
    }
    case Key_Asterisk: {
      return Keyboard::KEY_ASTERISK;
    }
    case Key_8: {
      return Keyboard::KEY_EIGHT;
    }
    case Key_ParenLeft: {
      return Keyboard::KEY_LEFT_PARENTHESIS;
    }
    case Key_9: {
      return Keyboard::KEY_NINE;
    }
    case Key_Equal: {
      return Keyboard::KEY_EQUAL;
    }
    case Key_Plus: {
      return Keyboard::KEY_PLUS;
    }
    case Key_Less: {
      return Keyboard::KEY_LESS_THAN;
    }
    case Key_Comma: {
      return Keyboard::KEY_COMMA;
    }
    case Key_Underscore: {
      return Keyboard::KEY_UNDERSCORE;
    }
    case Key_Minus: {
      return Keyboard::KEY_MINUS;
    }
    case Key_Greater: {
      return Keyboard::KEY_GREATER_THAN;
    }
    case Key_Period: {
      return Keyboard::KEY_PERIOD;
    }
    case Key_Semicolon: {
      return Keyboard::KEY_SEMICOLON;
    }
    case Key_Question: {
      return Keyboard::KEY_QUESTION;
    }
    case Key_Slash: {
      return Keyboard::KEY_SLASH;
    }
    case Key_BraceLeft: {
      return Keyboard::KEY_LEFT_BRACE;
    }
    case Key_BracketLeft: {
      return Keyboard::KEY_LEFT_BRACKET;
    }
    case Key_Bar: {
      return Keyboard::KEY_BAR;
    }
    case Key_Backslash: {
      return Keyboard::KEY_BACK_SLASH;
    }
    case Key_BraceRight: {
      return Keyboard::KEY_RIGHT_BRACE;
    }
    case Key_BracketRight: {
      return Keyboard::KEY_RIGHT_BRACKET;
    }
    case Key_QuoteDbl: {
      return Keyboard::KEY_QUOTE;
    }
    case Key_QuoteLeft: {
      return Keyboard::KEY_APOSTROPHE;
    }
    case Key_Hyper_L: 
    case Key_Hyper_R: {
      return Keyboard::KEY_HYPER;
    }
    case Key_A: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_A : Keyboard::KEY_A;
    }
    case Key_B: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_B : Keyboard::KEY_B;
    }
    case Key_C: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_C : Keyboard::KEY_C;
    }
    case Key_D: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_D : Keyboard::KEY_D;
    }
    case Key_E: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_E : Keyboard::KEY_E;
    }
    case Key_F: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_F : Keyboard::KEY_F;
    }
    case Key_G: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_G : Keyboard::KEY_G;
    }
    case Key_H: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_H : Keyboard::KEY_H;
    }
    case Key_I: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_I : Keyboard::KEY_I;
    }
    case Key_J: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_J : Keyboard::KEY_J;
    }
    case Key_K: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_K : Keyboard::KEY_K;
    }
    case Key_L: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_L : Keyboard::KEY_L;
    }
    case Key_M: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_M : Keyboard::KEY_M;
    }
    case Key_N: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_N : Keyboard::KEY_N;
    }
    case Key_O: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_O : Keyboard::KEY_O;
    }
    case Key_P: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_P : Keyboard::KEY_P;
    }
    case Key_Q: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_Q : Keyboard::KEY_Q;
    }
    case Key_R: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_R : Keyboard::KEY_R;
    }
    case Key_S: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_S : Keyboard::KEY_S;
    }
    case Key_T: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_T : Keyboard::KEY_T;
    }
    case Key_U: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_U : Keyboard::KEY_U;
    }
    case Key_V: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_V : Keyboard::KEY_V;
    }
    case Key_W: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_W : Keyboard::KEY_W;
    }
    case Key_X: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_X : Keyboard::KEY_X;
    }
    case Key_Y: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_Y : Keyboard::KEY_Y;
    }
    case Key_Z: {
      return (event->modifiers() & Qt::ShiftModifier) ? Keyboard::KEY_CAPITAL_Z : Keyboard::KEY_Z;
    }
    default: {
      return Keyboard::KEY_NONE;
    }
  }
}

#endif
