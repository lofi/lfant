#include "gameplay.h"

////////////////////////////////////////////////////////////////////////////////
namespace LF {
    
  class Input {
  public:
    enum Sources {
      MOUSE=0,
      GAMEPAD,
      KEYBOARD
    };

    enum GamepadSticks {
      GAMEPAD_LEFT_STICK=0,
      GAMEPAD_RIGHT_STICK,
      GAMEPAD_LEFT_STICK_X,
      GAMEPAD_LEFT_STICK_X_NEG,
      GAMEPAD_LEFT_STICK_Y,
      GAMEPAD_LEFT_STICK_Y_NEG,
      GAMEPAD_RIGHT_STICK_X,
      GAMEPAD_RIGHT_STICK_X_NEG,
      GAMEPAD_RIGHT_STICK_Y,
      GAMEPAD_RIGHT_STICK_Y_NEG
    };

    enum GamepadTriggers {
      GAMEPAD_LEFT_TRIGGER=0,
      GAMEPAD_RIGHT_TRIGGER
    };

    typedef struct {
      float x;
      float y;
      float z;
    } inputVectorf_t;

    typedef struct {
      int x;
      int y;
      int z;
    } inputVectori_t;

    typedef union {
      bool pressed;
      inputVectori_t ivalues;
      inputVectorf_t fvalues;
    } inputEventData_t;

    struct Event {
      Source source;
      inputEventData_t data;
    };
    
    static void bind(const int& command, const BIND_SOURCE& src, const int& item=0) {
      // pass
    }
  }; // end of class Input

  class Command
  {
  public:
    void addDelegate(std::function<void (const LF::Input::Event&)> delegate_fn) {
      // pass
    }
  };
    
#define LOFI_COMMAND(x) Command x
  
} // end of namespace LF
////////////////////////////////////////////////////////////////////////////////

// Commands are created globally
LOFI_COMMAND( look );
LOFI_COMMAND( jump );
LOFI_COMMAND( move );
LOFI_COMMAND( move_left );
LOFI_COMMAND( move_right );
LOFI_COMMAND( move_forward );
LOFI_COMMAND( move_backward );

int main(int argc, char *argv[]) {

  LF::Input::bind( look, LF::Input::MOUSE );
  LF::Input::bind( look, LF::Input::GAMEPAD, LF::Input::GAMEPAD_RIGHT_STICK);

  LF::Input::bind( move, LF::Input::GAMEPAD, LF::Input::GAMEPAD_LEFT_STICK );
  LF::Input::bind( move_left, LF::Input::KEYBOARD, gameplay::Keyboard::KEY_A );
  LF::Input::bind( move_right, LF::Input::KEYBOARD, gameplay::Keyboard::KEY_D );
  LF::Input::bind( move_forward, LF::Input::KEYBOARD, gameplay::Keyboard::KEY_W );
  LF::Input::bind( move_backward, LF::Input::KEYBOARD, gameplay::Keyboard::KEY_S );

  LF::Input::bind( jump, LF::Input::GAMEPAD, gameplay::Gamepad::BUTTON_X );

  ///////////

  float look_yaw=0.0f;
  float look_pitch=0.0f;

  look.addDelegate( [&look_yaw, &look_pitch] (const LF::Input::Event& event) {
      static float axis_multiplier = -2.0f;
      static float mouse_multiplier = 0.05f;
      
      switch(event.source) {

        case LF::Input::MOUSE: {
          look_yaw = MATH_DEG_TO_RAD( mouse_multiplier * event.data.ivalues.x );
          look_pitch = MATH_DEG_TO_RAD( mouse_multiplier * event.data.ivalues.y );
          break;
        }

        case LF::Input::GAMEPAD: {
          look_yaw = MATH_DEG_TO_RAD( axis_multiplier * event.data.fvalues.x );
          look_pitch = MATH_DEG_TO_RAD( axis_multiplier * event.data.fvalues.y );
          break;
        }

        default: break;
      }
    });
  
  return 0;
}
