
#ifndef LF_WINDOW_H_
#define LF_WINDOW_H_

#include <QtGui/QWindow>
#include <QtGui/QOpenGLFunctions>
#include <QtGui/QCursor>

class QEvent;

namespace LF {
  class Window : public QWindow, protected QOpenGLFunctions {
    Q_OBJECT
  public:
    explicit Window(QWindow* parent=0);
    ~Window();

    void initialize();
    void setEventCallback(std::function<bool (QEvent*)> cb);

    void setCursorVisible(bool visible);

  protected:
    bool event(QEvent *event);

  private:
    std::function<bool (QEvent*)> m_event_callback;
    QCursor* m_blank_cursor;
    QCursor* m_default_cursor;
  };
}

#endif // LF_WINDOW_H_
