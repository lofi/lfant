#ifndef MESHGAME_H_
#define MESHGAME_H_

#include "gameplay.h"
#include "lofi.h"

using namespace gameplay;

/**
 * Sample game for rendering a scene with a model/mesh.
 */
class MeshGame: public LF::World
{
public:
  
  MeshGame();
  virtual ~MeshGame();
  
protected:
  
  virtual void initialize();
  virtual void finalize();
  
  virtual void update(float elapsed_time);
  
  //virtual void preRender(float elapsed_time);
  virtual void postRender(float elapsed_time);
  virtual void loadingScreen(void* param);
  
private:
  
  void initBindings();
  
  bool drawScene(Node* node);
  
  void drawFrameRate(Font* font, const Vector4& color, unsigned int x, unsigned int y, unsigned int fps);
  
  float m_look_yaw;
  float m_look_pitch;
  Font* _font;
  Node* _modelNode;
  Node* _cam_base;
  Node* _cam_y_node;
  Node* _cam_x_node;
  Vector3 _keyboard_movement;
  Vector3 _gamepad_movement;
  gameplay::Game* _game;
};

#endif
