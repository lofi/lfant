#include <gameplay.h>
#include <lofi.h>

#include "MeshGame.h"

std::function<void (LF::Client*)> LF::Client::startup = [] (LF::Client* client) {
  std::shared_ptr<World> world( new MeshGame );
  client->setWorld( world );
};

std::function<void (LF::Client*)> LF::Client::shutdown = [] (LF::Client* client) {
  gameplay::print("SHUTDOWN!");
};

