#include "MeshGame.h"

using LF::Input;
using LF::Event;

LOFI_COMMAND(look);
LOFI_COMMAND(move);
LOFI_COMMAND(move_left);
LOFI_COMMAND(move_right);
LOFI_COMMAND(move_forward);
LOFI_COMMAND(move_backward);
LOFI_COMMAND(quit);

MeshGame::MeshGame()
  : m_look_yaw(0.0f),
    m_look_pitch(0.0f),
    _font(nullptr), 
    _modelNode(nullptr), 
    _cam_base(nullptr),
    _cam_y_node(nullptr),
    _cam_x_node(nullptr),
    _game(nullptr)
{
  _game = gameplay::Game::getInstance();
}

MeshGame::~MeshGame()
{
}

void MeshGame::initialize()
{
  // Load font
  _font = Font::create("res/arial40.gpb");
  
  // Load mesh/scene from file
  Bundle* bundle = Bundle::create("res/duck.gpb");
  m_scene = bundle->loadScene();
  SAFE_RELEASE(bundle);
  
  // Get the duck node
  _modelNode = m_scene->findNode("duck");
  
  // Bind the material to the model
  _modelNode->getModel()->setMaterial("res/duck.material");
  
  // Find the light node
  Node* lightNode = m_scene->findNode("directionalLight1");
  
  // Bind the light node's direction into duck's material.
  _modelNode->getModel()->getMaterial()->getParameter("u_lightDirection")->bindValue(lightNode, &Node::getForwardVectorView);
  
  // Update the aspect ratio for our scene's camera to match the current device resolution
  m_scene->getActiveCamera()->setAspectRatio((float)_game->getWidth() / (float)_game->getHeight());
  
  _game->setMouseCaptured(true);
  
  Camera* cam = m_scene->getActiveCamera();
  _cam_base = Node::create();
  _cam_y_node = Node::create();
  _cam_x_node = cam->getNode();
  
  _cam_base->setTranslation( _cam_x_node->getTranslation() );
  _cam_x_node->setTranslation(0.0f, 0.0f, 0.0f);
  
  m_scene->addNode(_cam_base);
  _cam_base->addChild(_cam_y_node);
  _cam_y_node->addChild(_cam_x_node);

  initBindings();

  // draw a reference plane
  enableGrid(true);
}

void MeshGame::finalize()
{ 
  SAFE_RELEASE(_font);
}

void MeshGame::initBindings() {
  LF::Input::bind( look, LF::MOUSE_MOTION);
  LF::Input::bind( look, LF::GAMEPAD, LF::GAMEPAD_RIGHT_STICK );
  look.addDelegate( [=] (Event* event) {
      switch(event->source) {
        
        case LF::MOUSE_MOTION: {
          _cam_y_node->rotateY( MATH_DEG_TO_RAD(0.05f * event->data.ivalues.x) );
          _cam_x_node->rotateX( MATH_DEG_TO_RAD(0.05f * event->data.ivalues.y) );
        } break;

        case LF::GAMEPAD: {
          _cam_y_node->rotateY( MATH_DEG_TO_RAD(-2.0f * event->data.fvalues.x) );
          _cam_x_node->rotateX( MATH_DEG_TO_RAD(-2.0f * event->data.fvalues.y) );
        } break;

        default: {} break;
      }
    });
  
  LF::Input::bind( move, LF::GAMEPAD, LF::GAMEPAD_LEFT_STICK );
  LF::Input::bind( move_left, LF::KEYBOARD, gameplay::Keyboard::KEY_S );
  LF::Input::bind( move_right, LF::KEYBOARD, gameplay::Keyboard::KEY_F );
  LF::Input::bind( move_backward, LF::KEYBOARD, gameplay::Keyboard::KEY_D );
  LF::Input::bind( move_forward, LF::KEYBOARD, gameplay::Keyboard::KEY_E );
  move.addDelegate( [=] (Event* event) {
      switch(event->source) {
        case LF::GAMEPAD: {
          _gamepad_movement.x = 0.25f * event->data.fvalues.x;
          _gamepad_movement.z = 0.25f * event->data.fvalues.y;
        } break;
        default: {} break;
      }
    });
  move_left.addDelegate( [=] (Event* event) {
      _keyboard_movement.x = (event->data.pressed) ? -0.25f : 0.0f;
    });
  move_right.addDelegate( [=] (Event* event) {
      _keyboard_movement.x = (event->data.pressed) ? 0.25f : 0.0f;
    });
  move_forward.addDelegate( [=] (Event* event) {
      _keyboard_movement.z = (event->data.pressed) ? -0.25f : 0.0f;
    });
  move_backward.addDelegate( [=] (Event* event) {
      _keyboard_movement.z = (event->data.pressed) ? 0.25f : 0.0f;
    });

  // DEVELOPER bailout setup
  LF::Input::bind( quit, LF::KEYBOARD, gameplay::Keyboard::KEY_ESCAPE );
  LF::Input::bind( quit, LF::GAMEPAD, gameplay::Gamepad::BUTTON_MENU2 );
  quit.addDelegate( [=] (Event* event) {
      _game->exit();
    });
}

void MeshGame::update(float elapsedTime)
{
  static float rotate_speed = 0.05f;
  static Camera* camera = m_scene->getActiveCamera(); 
  
  Vector3 final_movement = _keyboard_movement + _gamepad_movement;
  _cam_y_node->getWorldMatrix().transformVector(&final_movement);
  _cam_base->translate(final_movement);

  // Rotate the ducky
  _modelNode->rotateY(elapsedTime * MATH_DEG_TO_RAD(0.05f));
}

void MeshGame::postRender(float elapsedTime)
{
  // Draw the fps
  drawFrameRate(_font, gameplay::Vector4(0, 0.5f, 1, 1), 5, 1, _game->getFrameRate());
}

bool MeshGame::drawScene(Node* node)
{
  Model* model = node->getModel();
  if (model)
    model->draw();
  return true;
}

void MeshGame::drawFrameRate(Font* font, const Vector4& color, unsigned int x, unsigned int y, unsigned int fps)
{
  char buffer[10];
  sprintf(buffer, "%u", fps);
  font->start();
  font->drawText(buffer, x, y, color, font->getSize());
  font->finish();
}

void MeshGame::loadingScreen(void* param)
{
  _game->clear(gameplay::Game::CLEAR_COLOR_DEPTH, gameplay::Vector4(0, 0, 0, 1), 1.0f, 0);
  SpriteBatch* batch = SpriteBatch::create("res/logo_powered_white.png");
  batch->start();
  batch->draw(_game->getWidth() * 0.5f, _game->getHeight() * 0.5f, 0.0f, 512.0f, 512.0f, 0.0f, 1.0f, 1.0f, 0.0f, Vector4::one(), true);
  batch->finish();
  SAFE_DELETE(batch);
}
