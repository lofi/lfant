#ifndef LF_APPLICATION_H_
#define LF_APPLICATION_H_

#include <vector>
#include <string>

#include <Poco/Util/Application.h>
#include <Poco/Util/OptionSet.h>

namespace gameplay {
  class Game;
}

class LFApplication : public Poco::Util::Application {
public:
  LFApplication();
  virtual ~LFApplication();

protected:
  int main(const std::vector<std::string>& args);
  void initialize(Poco::Util::Application &self);
  void uninitialize();

  void defineOptions(Poco::Util::OptionSet &options);
  void handleOption(const std::string &name, const std::string &value);
  void displayHelp();

  void initLogging();
  void setConfigurationDefaults();
  int loadConfiguration();
};

#endif // LF_APPLICATION_H_
